# !/bin/bash

input=${1:-/dev/stdin}

# Find the best ARI
# echo -e "#Method\tAlpha\tBeta\tGamma\tDelta\tAlphan\tBetan\tGamman\tWeights\tKclusters\tGroupings\tARI"
awk -F $'\t' -v max=0 '$1 ~ /single|complete|average|weighted/ {if($12>max){line=$0; max=$12}}END{print line}' $input
