#!/usr/bin/env python2
import sys
import numpy as np
import scipy.cluster.hierarchy as hac
import matplotlib.pyplot as plt
import scipy.spatial.distance as distance
import scipy.stats as stats
import sklearn.metrics as metrics
import colorsys
import itertools
from os.path import basename
import string
import argparse
import time
import multiprocessing
from random import randint

legendfonttsize=10

class ncolors(object):
	"""docstring for ncolors"""
	def __init__(self, N):
		super(ncolors, self).__init__()
		self.colors = ["#000000", "#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#00FFFF", "#FF00FF", "#808080", "#FF8080", "#80FF80", "#8080FF", "#008080", "#800080", "#808000",
		"#FFFF80", "#80FFFF", "#FF80FF", "#FF0080", "#80FF00", "#0080FF", "#00FF80", "#8000FF", "#FF8000", "#000080", "#800000", "#008000", "#404040", "#FF4040", "#40FF40",
		 "#4040FF", "#004040", "#400040", "#404000", "#804040", "#408040", "#404080", "#FFFF40", "#40FFFF", "#FF40FF", "#FF0040", "#40FF00", "#0040FF", "#FF8040", "#40FF80",
		 "#8040FF", "#00FF40", "#4000FF", "#FF4000", "#000040", "#400000", "#004000", "#008040", "#400080", "#804000", "#80FF40", "#4080FF", "#FF4080", "#800040", "#408000",
			"#004080", "#808040", "#408080", "#804080", "#C0C0C0", "#FFC0C0", "#C0FFC0", "#C0C0FF", "#00C0C0", "#C000C0", "#C0C000", "#80C0C0", "#C080C0", "#C0C080", "#40C0C0",
			"#C040C0", "#C0C040", "#FFFFC0", "#C0FFFF", "#FFC0FF", "#FF00C0", "#C0FF00", "#00C0FF", "#FF80C0", "#C0FF80", "#80C0FF", "#FF40C0", "#C0FF40", "#40C0FF", "#00FFC0",
			"#C000FF", "#FFC000", "#0000C0", "#C00000", "#00C000", "#0080C0", "#C00080", "#80C000", "#0040C0", "#C00040", "#40C000", "#80FFC0", "#C080FF", "#FFC080", "#8000C0",
			"#C08000", "#00C080", "#8080C0", "#C08080", "#80C080", "#8040C0", "#C08040", "#40C080", "#40FFC0", "#C040FF", "#FFC040", "#4000C0", "#C04000", "#00C040", "#4080C0",
			"#C04080", "#80C040", "#4040C0", "#C04040", "#40C040", "#202020", "#FF2020", "#20FF20"]

	def all(self):
		return self.colors

	def get(self, i):
		return self.colors[i]

def weighted_euclidean_distance(X, Y, weights):
	return distance.cdist(X[np.newaxis,:],Y[np.newaxis,:],'euclidean', weights)[0,0]

def normalize(D):
	max = np.amax(D)
	D /= max
	return D

def agg_distance(X, Y, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, weights):
	d1,d2,d3,d4,d5,d6,d7 = 0,0,0,0,0,0,0

	if eucl > 0:
		d1  = weighted_euclidean_distance(X, Y, weights)

	if eucld > 0:
		diffX, diffY = np.diff(X), np.diff(Y)
		d2           = weighted_euclidean_distance(diffX, diffY, weights)

	if corrp > 0:
		x,y = list(X),list(Y)
		x *= weights
		y *= weights
		d3 = 1 - stats.pearsonr(x, y)[0]

	if corrs > 0:
		x,y = list(X),list(Y)
		x *= weights
		y *= weights
		d4 = 1 - stats.spearmanr(x, y)[0]

	if eucln > 0:
		x,y = list(X),list(Y)
		x,y = normalize(x), normalize(y)
		d5 = weighted_euclidean_distance(x, y, weights)

	if eucldn > 0:
		x,y = list(X),list(Y)
		x,y = normalize(x), normalize(y)
		diffX, diffY = np.diff(x), np.diff(y)
		d6           = weighted_euclidean_distance(diffX, diffY, weights)

	if corrpn > 0:
		x,y = list(X),list(Y)
		x,y = normalize(x), normalize(y)
		x *= weights
		y *= weights
		d7  = 1 - stats.pearsonr(x, y)[0]

	return (eucl * d1) + (eucld * d2) + (corrp * d3) + (corrs * d4) + (eucln * d5) + (eucldn * d6) + (corrpn * d7)

def read_weights(file):
	files = np.loadtxt(file, comments="#", dtype=str, ndmin=1) # Important to keep array of at least 1 dim to avoid unpacking on a single line.
	data = {f:np.loadtxt(f) for f in files}
	return data

def read_agg(file):
	# We are only interested in the signal value column, therefore
	# only read the second column.
	return np.loadtxt(file, comments="#", usecols=[1])

def read_data(file):
	files,labels = np.loadtxt(file, comments="#", dtype=str, unpack=True)
	data         = [read_agg(files[i]) for i in xrange(0,len(files))]
	files        = [basename(f) for f in files]
	return files, data, labels

def distance_matrix(data, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, weights):
	return distance.pdist(data, lambda X,Y: agg_distance(X, Y, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, weights))

def run_hac(matrix, method, K):
	Z = hac.linkage(matrix, method=method)
	C = hac.fcluster(Z, K, 'maxclust')
	return Z,C

def plot_data(data, files, labels):
	ulabels = np.unique(labels)
	colors  = ncolors(len(ulabels)) # one color for each unique label
	lcolors = {ulabels[i]:colors.get(i) for i in xrange(len(ulabels))}

	f = plt.figure(figsize=(20,10))
	ax = plt.subplot()
	ax.set_title("Data")
	for d,i in zip(data, xrange(0,len(data))):
		# use the filenames as curve labels
		# plot identically labeled curved with the same color
		ax.plot(d, label=files[i], color=lcolors[labels[i]])

	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=legendfonttsize, ncol=2)

	return f

def plot_dendrodram(Z, C, labels, k, title):
	dlabels = [labels[i] + ':' + str(C[i]) for i in xrange(len(labels))]
	f = plt.figure(figsize=(20,10))
	ax = plt.subplot()
	ax.set_title("Dendrogram - " + title)
	ct=Z[-(k-1),2] # calculate color threshold
	hac.dendrogram(Z, labels=dlabels, leaf_font_size=9, leaf_rotation=90, color_threshold=ct)
	return f

def plot_clusters(data, C, k, labels, title):
	colors = ncolors(k)
	f = plt.figure(figsize=(20,10))
	ax = plt.subplot()
	ax.set_title("Clusters - " + title)
	for d,i in zip(data,xrange(0, len(data))):
		ax.plot(d, label=labels[i], color=colors.get(C[i]-1))

	# Shrink current axis by 20%
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

	# Put a legend to the right of the current axis
	ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=legendfonttsize, ncol=2)

	return f

def ARI(labels_true, labels_clusters):
	return metrics.adjusted_rand_score(labels_true, labels_clusters)

def print_header(labels):
	# TODO : A max of 26 clusters can be rep here ...
	print "#Labels:" + ",".join(labels)
	alphabet = list(string.ascii_lowercase)
	ulabels = np.unique(labels)
	associations = {ulabels[i]:alphabet[i] for i in xrange(len(ulabels))}
	clusterslbls = " ".join([associations[labels[i]] for i in xrange(len(labels))])
	print "#Method\teucl\teucld\tcorrP\tcorrS\teucln\teucldn\tcorrPn\tWeights\tKclusters\t{0}\tARI".format(clusterslbls)
	sys.stdout.flush()

def format_results(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, wfile, kclusters, clusters, ari):
	fmt = " ".join(str(c) for c in clusters.tolist())
	return "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}".format(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, wfile, kclusters, fmt, ari)

def print_results(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, wfile, kclusters, clusters, ari):
	print format_results(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, wfile, kclusters, clusters, ari)

def process_all(allweights, allmetrics, allmethods, data, labels, args):
	for wfname, weights in allweights.iteritems():
		for eucl, eucld, corrp, corrs, eucln, eucldn, corrpn in allmetrics:
			# Compute similarties matrix
			matrix = distance_matrix(data, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, weights)

			# Run clustering for all 4 methods and plot results
			for method in allmethods:
				title = "m({0}) a({1}) b({2}) g({3}) d({4}) an({5} bn({6}) gn({7}) w({8}) k({9})".format(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, basename(wfname), args.kclusters)

				# do clustering with k-clusters
				Z,C = run_hac(matrix, method, args.kclusters)

				# Draw the resulting dendrograms with the files names as labels
				if(args.plotdendrogram):
					plot_dendrodram(Z, C, labels, args.kclusters, title).savefig(title+"_dendrogram.png", dpi=args.dpi)

				# Plot resulting clusters
				if(args.plotclusters):
					plot_clusters(data, C, args.kclusters, files, title).savefig(title+"_clusters.png", dpi=args.dpi)

				# Compute Ajdusted Rand Index for cluster sanity
				ari = ARI(labels, C)

				# Print to console the results
				print_results(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, basename(wfname), args.kclusters, C, ari)

				plt.close("all") # Closes all figures once saved in order to release memory gained by figures

# TODO: Add parallelism to the metrics and methods loops, one could call --processes 684 to have each result computed in a thread
def process_weight_fct(wfname, weights, allmetrics, allmethods, data, labels, args):
	results = []
	for eucl, eucld, corrp, corrs, eucln, eucldn, corrpn in allmetrics:
		# Compute similarties matrix
		matrix = distance_matrix(data, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, weights)

		# Run clustering for all 4 methods and plot results
		for method in allmethods:
			title = "m({0}) a({1}) b({2}) g({3}) d({4}) an({5} bn({6}) gn({7}) w({8}) k({9})".format(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, wfname, args.kclusters)

			# do clustering with k-clusters
			Z,C = run_hac(matrix, method, args.kclusters)

			# Draw the resulting dendrograms with the files names as labels
			if(args.plotdendrogram):
				plot_dendrodram(Z, C, labels, args.kclusters, title).savefig(title+"_dendrogram.png", dpi=args.dpi)

			# Plot resulting clusters
			if(args.plotclusters):
				plot_clusters(data, C, args.kclusters, files, title).savefig(title+"_clusters.png", dpi=args.dpi)

			# Compute Ajdusted Rand Index for cluster sanity
			ari = ARI(labels, C)

			plt.close("all") #

			# Print to console the results
			results.append(format_results(method, eucl, eucld, corrp, corrs, eucln, eucldn, corrpn, basename(wfname), args.kclusters, C, ari))
	return results

def main(args):
	# Read data
	allweights          = read_weights(args.weightsfile)
	files, data, labels = read_data(args.aggsfile) # Extract agg data and labels of files

	assert(len(data) == len(files))
	assert(len(data) == len(labels))

	print_header(labels)

	# Plot all data into one graph
	if(args.plotdata):
		plot_data(data, files, labels).savefig("data.png", dpi=args.dpi)

	# Generate all parameters combinations for eucl, eucln, eucld, eucldn, corrp, corrpn, corrs
	if args.testall:
		allmetrics = [(1,0,0,0,0,0,0),(0,1,0,0,0,0,0),(0,0,1,0,0,0,0),(0,0,0,1,0,0,0),(0,0,0,0,1,0,0),(0,0,0,0,0,1,0),(0,0,0,0,0,0,1),(1,0,1,0,0,0,0),(1,0,0,1,0,0,0),(1,0,0,0,0,0,1),(0,1,1,0,0,0,0),(0,1,0,1,0,0,0),(0,1,0,0,0,0,1),(0,0,1,0,1,0,0),(0,0,0,1,1,0,0),(0,0,0,0,1,0,1),(0,0,1,0,0,1,0),(0,0,0,1,0,1,0),(0,0,0,0,0,1,1)]
	else:
		allmetrics = [(int("eucl" in args.metrics), int("eucld" in args.metrics), int("corrp" in args.metrics), int("corrs" in args.metrics), int("eucln" in args.metrics), int("eucldn" in args.metrics), int("corrpn" in args.metrics))]

	# Generate all methods to run
	if args.testall:
		allmethods = ["single", "complete", "average", "weighted"]
	else:
		allmethods = args.methods

	if args.processes > 1:
		results = []
		pool = multiprocessing.Pool(processes=args.processes) # Create a pool of N process

		# Apply asynchronously each function to a process
		for wfname, weights in allweights.iteritems():
			results.append(pool.apply_async(process_weight_fct, args=((wfname, weights, allmetrics, allmethods, data, labels, args))))

		pool.close()
		pool.join() #Wait for all jobs

		for result in results:
			for value in result.get():
				print value
	else:
		process_all(allweights, allmetrics, allmethods, data, labels, args)

def check_kcluster(value):
	ival = int(value)
	if ival < 1 or ival > 26:
		raise argparse.ArgumentTypeError("%s is an invalid number of clusters. Expecting > 0 and < 27." % value)
	return ival

def check_negatives(value):
	ival = int(value)
	if ival < 0:
		raise argparse.ArgumentTypeError("%s is an invalid value. Expecting positive value." % value)
	return ival

if __name__ == "__main__":
	# Usage
	#
	# Testing:
	# python vap_sim.py --aggsfile aggs_file.txt --weightsfile weights_file.txt --testall
	# python vap_sim.py --aggsfile aggs_file.txt --weightsfile weights_file.txt -k 5 --testall
	#
	# Specify the methods and the metrics
	# python vap_sim.py --aggsfile aggs_file.txt --weightsfile weights_file.txt -k 5 --metrics eucldn --methods single
	# python vap_sim.py --aggsfile aggs_file.txt --weightsfile weights_file.txt -k 5 --metrics eucldn corrs --methods single average

	# TODO: Include option for positive and negative filtering of aggs and weights, from cmd and file (--exclude-aggs-from, --exclude-weights-from, --exclude-weights [list])
	# TODO: sort labels
	# TODO: Use uniform weights when no weightfile is provided
	# TODO: Remove `corrpn` metric as results are always identical to `corrp`
	parser = argparse.ArgumentParser(description="Identification of similar aggregate profiles")

	# add arguments
	parser.add_argument("--aggsfile", required=True, help="Specify a file containing a list of Verstaile Aggregate Profiles aggregate data files and their label.")
	parser.add_argument("--weightsfile", required=True, help="Specify a file containing a list of weights files")
	parser.add_argument("-k","--kclusters", type=check_kcluster, help="Number of desired clusters (default: %(default)s)", default=1)
	parser.add_argument("--plotdata", action="store_true", help="Turn on plotting of data.")
	parser.add_argument("--plotdendrogram", action="store_true", help="Turn on plotting of dendrogram.")
	parser.add_argument("--plotclusters", action="store_true", help="Turn on plotting of clusters.")
	parser.add_argument("--dpi", type=check_negatives, default=50, help="Number of DPI for plotting (default: %(default)s).")
	parser.add_argument("--methods", choices=["single", "complete", "average", "weighted"], nargs='+', default=["average"], help="Specify the methods (default: %(default)s).")
	parser.add_argument("--metrics", choices=["eucl", "eucld", "corrp", "corrs", "eucln", "eucldn", "corrpn"], nargs='+', default=["eucldn"], help="Specify the metrics (default: %(default)s).")
	parser.add_argument("--testall", action="store_true", help="Turn on testing of all combinations of parameters (can be long).")
	parser.add_argument("-p","--processes", type=check_negatives, default=1, help="Number of processes (default: %(default)s).")

	args = parser.parse_args()

	print "date:", time.strftime("%Y-%m-%d %H:%M:%S")
	print "aggs file:", args.aggsfile
	print "weights file:", args.weightsfile
	print "kclusters:", args.kclusters
	print "methods:", ["single", "complete", "average", "weighted"] if args.testall else args.methods
	print "metrics:", ["eucl", "eucld", "corrp", "corrs", "eucln", "eucldn", "corrpn"] if args.testall else  args.metrics
	print "plotdata:", args.plotdata
	print "plotdendrogram:", args.plotdendrogram
	print "plotclusters:", args.plotclusters
 	print "dpi:", args.dpi
	print "processes:", args.processes
	print "testall:", args.testall
	sys.stdout.flush()

	main(args)
