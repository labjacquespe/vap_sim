VAP Similarity : VAP Utils
=====================================
Copyright (C) 2017 Charles Coulombe
Université de Sherbrooke. All rights reserved.

A hierarchical agglomerative clustering (HAC) tool to identify similar [VAP](https://bitbucket.org/labjacquespe/vap) profiles.

# Usage
```
usage: vap_sim.py [-h] --aggsfile AGGSFILE --weightsfile WEIGHTSFILE
                  [-k KCLUSTERS] [--plotdata] [--plotdendrogram]
                  [--plotclusters] [--dpi DPI]
                  [--methods {single,complete,average,weighted} [{single,complete,average,weighted} ...]]
                  [--metrics {eucl,eucld,corrp,corrs,eucln,eucldn,corrpn} [{eucl,eucld,corrp,corrs,eucln,eucldn,corrpn} ...]]
                  [--testall] [-p PROCESSES]

Identification of similar aggregate profiles

optional arguments:
  -h, --help                                                    Show this help message and exit
  --aggsfile AGGSFILE                                           Specify a file containing a list of paths to Verstaile Aggregate Profiles aggregate data files and their labels.
  --weightsfile WEIGHTSFILE                                     Specify a file containing a list of weights files
  -k KCLUSTERS, --kclusters KCLUSTERS                           Number of desired clusters (default: 1)
  --plotdata                                                    Turn on plotting of data.
  --plotdendrogram                                              Turn on plotting of dendrogram.
  --plotclusters                                                Turn on plotting of clusters.
  --dpi DPI                                                     Number of DPI for plotting (default: 50).
  --methods [{single,complete,average,weighted} ...]            Specify the methods (default: ['average']).
  --metrics [{eucl,eucld,corrp,corrs,eucln,eucldn,corrpn} ...]  Specify the metrics (default: ['eucldn']).
  --testall                                                     Turn on testing of all combinations of  parameters (can be long).
  -p PROCESSES, --processes PROCESSES                           Number of processes (default: 1).
```

# Installation
The script require `numpy`, `sklearn.metrics` and `mathplotlib` to run.
```
git clone https://bitbucket.org/labjacquespe/vap_sim
```

# Run
```
python vap_sim.py --aggsfile aggsfiles.txt --weightsfile weightsfile.txt --kclusters 5 --metrics eucldn corrs --methods weighted --processes 5 > results.txt
```
where
- `aggsfiles.txt` file contains the list of paths to VAP aggregate data files and their labels.
**Note:** The file must be sorted by labels.
- `weightsfile` file contains the list of paths to weights functions data files.

# Determining the best parameters combinations
You can determine the best combination of parameters by running the script with the flag `--testall`. It will test 684 parameters combinations. You can then find the best one by running the following:
```
bash best.sh results.txt
```

# Determining the Adjusted Rand Index (ARI) averages
You can determine the ARI averages for each parameter by running the following script:
```
bash avgstd.sh results.txt
```

# Bug Reports and Feedback
You can report a bug [here](https://bitbucket.org/labjacquespe/vap_sim/issues).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

# Licensing
`vap_sim` is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt). For further details see the LICENSE file
in this directory.
