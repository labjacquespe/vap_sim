#!/bin/bash

input=${1:-/dev/stdin}

# TODO : Take weights file as list of weights functions names. Instead of uniform, etc.

echo "Variable Mean StdDev N"

# Methods complete, single, average, weighted
echo "Mehtods"
awk -F'\t' '$1 ~ /single/   { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "single",   sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$1 ~ /complete/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "complete", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$1 ~ /average/  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "average",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$1 ~ /weighted/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "weighted", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
echo ""

# Parameters eucl, eucld, corrp, corrs, eucln, eucldn, corrpn combinations
# Combinations exclude indivuals scores
echo "Metric Comb."
awk -F'\t' '$2 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucl",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$3 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucld",   sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$4 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "corrp",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$5 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "corrs",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$6 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucln", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$7 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucldn",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$8 ~ /1/ && ($2+$3+$4+$5+$6+$7+$8 == 2) { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "corrpn", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
echo ""

# Parameters eucl, eucld, corrp, corrs, eucln, eucldn, corrpn individually
echo "Metric Ind."
awk -F'\t' '$2 ~ /1/ && $3 ~ /0/ && $4 ~ /0/ && $5 ~ /0/ && $6 ~ /0/ && $7 ~/0/ && $8 ~ /0/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucl",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$2 ~ /0/ && $3 ~ /1/ && $4 ~ /0/ && $5 ~ /0/ && $6 ~ /0/ && $7 ~/0/ && $8 ~ /0/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucld",   sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$2 ~ /0/ && $3 ~ /0/ && $4 ~ /1/ && $5 ~ /0/ && $6 ~ /0/ && $7 ~/0/ && $8 ~ /0/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "corrp",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$2 ~ /0/ && $3 ~ /0/ && $4 ~ /0/ && $5 ~ /1/ && $6 ~ /0/ && $7 ~/0/ && $8 ~ /0/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "corrs",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$2 ~ /0/ && $3 ~ /0/ && $4 ~ /0/ && $5 ~ /0/ && $6 ~ /1/ && $7 ~/0/ && $8 ~ /0/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucln", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$2 ~ /0/ && $3 ~ /0/ && $4 ~ /0/ && $5 ~ /0/ && $6 ~ /0/ && $7 ~/1/ && $8 ~ /0/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "eucldn",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$2 ~ /0/ && $3 ~ /0/ && $4 ~ /0/ && $5 ~ /0/ && $6 ~ /0/ && $7 ~/0/ && $8 ~ /1/ { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "corrpn", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
echo ""

#Weights
echo "Weights"
awk -F'\t' '$9 == "uniform"    { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "U1",    sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian1"  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G1_1",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian13" { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G1_2", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian5"  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G1_3",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian3"  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G1_4",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian2"  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G2_1",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian4"  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G2_2",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian11" { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G2_3", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
awk -F'\t' '$9 == "gaussian6"  { n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "G2_4",  sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
echo ""

echo "Avg/Std"
awk -F'\t' '{ n++; sum += $12; sumsq += $12**2 } END { if (n > 0) print "avg/std", sum/n, sqrt(sumsq/n - (sum/n)**2), n }' $input
